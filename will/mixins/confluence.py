import logging
import json
import re
import requests

from will import settings
from ..utils import BasicRESTClient
from ..utils import key_gen

CONFLUENCE_SPACE_ENDPOINT = "/rest/api/space/%(id)s"
CONFLUENCE_USER_ENDPOINT = "/rest/api/user"
CONFLUENCE_BLUEPRINT_ENDPOINT = "/rest/create-dialog/1.0/space-blueprint/create-space"

class ConfluenceMixin(object):

    try:
        logging.debug('Attempting to get mixin settings from configuraiton')
        cdefault_user = settings.CONFLUENCE_USERNAME
        cdefault_pass = settings.CONFLUENCE_PASSWORD
        capp_root = settings.CONFLUENCE_SERVER

        cclient = BasicRESTClient(capp_root, cdefault_user, cdefault_pass)
    except AttributeError:
        logging.error('Parameter(s) missing from configuration; \
        CONFLUENCE requires CONFLUENCE_USERNAME, CONFLUENCE_PASSWORD, \
        and CONFLUENCE_SERVER.')

    def get_space(self, space_key=None):
        """ return information about a specific space
            :param space_key: confluence space key
            :return:
        """

        endpoint = CONFLUENCE_SPACE_ENDPOINT % {'id': str(space_key or '')}
        logging.info('Getting space with %(key)s from server %(server)s'
                      % {'key': space_key, 'server': self.capp_root})

        return self.cclient.request("GET", endpoint)

    def get_space_keys(self, space_name=None):
        """ return space keys; if a space_name is not provided all keys are
                returned
            :param: space_name: (optional) name of the space whose key will
                be returned
            :param user: (optional): user to act as the submitter
            :param password: (optional): password of the user acting
            :return: list of keys
        """

        if space_name:
            pass
        else:
            key_list = [r.get('key') for r in self.get_space(space_key=None)]

            return key_list

    def get_confluence_user(self, user_name):
        """ return information about a specific user; if no user is provided
            all user data will be returned
            :param user_name: confluence username of the user to retrieve
            :return: json object
        """
        endpoint = CONFLUENCE_USER_ENDPOINT
        params = {'username': user_name}

        logging.info('Getting user %(user)s from server %(server)s'
                      % {'user': user_name, 'server': self.capp_root})


        return self.cclient.request("GET", endpoint, params=params)

    def create_space(self, space_name, space_key=None, description=None,
                     space_admin=None, blueprint=False, **kwargs):

        """
            :param space_name: name of the space to create
            :param space_key: (optional) pre-defined confluence spacke key; if
                not provided one will be generated from the space name
            :param space_admin: (optional) confluence username of the user to
                assign as the admin in the space
            :param blueprint: (optional) confluence blueprint to use as the
                template for the space; if true, context_element, and
                blueprint_id must be provided as a keyword arg
            :return: json response object

        """

        if space_key is None:
            # Confluence keys are maxed at 255 chars
            space_key = utils.key_gen(space_name, 255,
                                            self.space_key_exists)
            logging.debug('Generated Confluence Space Key: %s' % space_key)

        if blueprint and kwargs.get('context_element'):
            # do blueprint things


            data = {"spaceKey": space_key, "name": space_name,
                    "description": description,
                    "spaceBlueprintId": kwargs.get('blueprint_id'),
                    "context": kwargs.get('context_element')}


            endpoint = CONFLUENCE_BLUEPRINT_ENDPOINT

        else:

            data = {"key": space_key, "name": project_name}

            pass

        logging.debug('Creating project: %(data)s' % {'data': data})
        return self.cclient.request("POST", endpoint=endpoint,
                                    data=json.dumps(data))


    def space_key_exists(self, space_key):
        """ checks whether the provided key has been used for a project
            :param proj_key: project key to validate
            :retrun boolean
        """

        try:

           r = self.get_space(space_key)
        except requests.exceptions.HTTPError as e:
            # 404 indicates the project doesn't exist
            if e.response.status_code == 404:
                return False
            else:
                raise

        return True
